<?php
// HTTP
define('HTTP_SERVER', 'http://loja.insigndigital.com.br/');
define('HTTP_IMAGE', 'http://loja.insigndigital.com.br/image/');
define('HTTP_ADMIN', 'http://loja.insigndigital.com.br/admin/');

// HTTPS
define('HTTPS_SERVER', 'http://loja.insigndigital.com.br/');
define('HTTPS_IMAGE', 'http://loja.insigndigital.com.br/image/');

// DIR
define('DIR_APPLICATION', '/home/heronreis/loja.insigndigital.com.br/catalog/');
define('DIR_SYSTEM', '/home/heronreis/loja.insigndigital.com.br/system/');
define('DIR_DATABASE', '/home/heronreis/loja.insigndigital.com.br/system/database/');
define('DIR_LANGUAGE', '/home/heronreis/loja.insigndigital.com.br/catalog/language/');
define('DIR_TEMPLATE', '/home/heronreis/loja.insigndigital.com.br/catalog/view/theme/');
define('DIR_CONFIG', '/home/heronreis/loja.insigndigital.com.br/system/config/');
define('DIR_IMAGE', '/home/heronreis/loja.insigndigital.com.br/image/');
define('DIR_CACHE', '/home/heronreis/loja.insigndigital.com.br/system/cache/');
define('DIR_DOWNLOAD', '/home/heronreis/loja.insigndigital.com.br/download/');
define('DIR_LOGS', '/home/heronreis/loja.insigndigital.com.br/system/logs/');

// DB
define('DB_DRIVER', 'mysql');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'ips_loja');
define('DB_PREFIX', '');
?>