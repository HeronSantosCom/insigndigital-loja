<?php
// Heading
$_['heading_title']      = 'Bank Transfer <img src="../image/bancocef.jpg" />';

// Text 
$_['text_payment']       = 'Payment';
$_['text_success']       = 'Success: You have modified bank transfer CEF details!';

// Entry
$_['entry_bank']         = 'Bank Transfer CEF Instructions:';
$_['entry_order_status'] = 'Order Status:';
$_['entry_geo_zone']     = 'Geo Zone:';
$_['entry_status']       = 'Status:';
$_['entry_sort_order']   = 'Sort Order:';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify payment bank transfer CEF!';
$_['error_bank']         = 'Bank Transfer Instructions Required!';
?>