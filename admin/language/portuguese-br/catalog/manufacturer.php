<?php
// Heading
$_['heading_title']		= 'Fabricantes';

// Text
$_['text_success']		= 'Fabricante atualizado com sucesso!';
$_['text_default']		= 'Loja Padrão';
$_['text_image_manager']= 'Gerenciar imagens';
$_['text_percent']       = 'Porcentagem';
$_['text_amount']        = 'Valor fixo';


// Column

$_['column_name']		= 'Nome do fabricante';
$_['column_sort_order']	= 'Ordem';
$_['column_action']		= 'Ação';


// Entry
$_['entry_name']		= 'Nome do fabricante:';
$_['entry_store']		= 'Lojas:';
$_['entry_keyword']		= 'URL amigável:';
$_['entry_image']		= 'Imagem:';
$_['entry_sort_order']	= 'Ordem:';
$_['entry_type']         = 'Tipo:';



// Error

$_['error_permission']	= 'Atenção: Você não possui permissão para alterar os fabricantes!';
$_['error_name']		= 'O nome do fabricante deve ter entre 3 a 64 caracteres!';
$_['error_product']		= 'Atenção: este fabricante não pode ser removido pois está atribuído a %s produto(s)!';
?>
