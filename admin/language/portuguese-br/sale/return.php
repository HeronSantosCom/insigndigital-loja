<?php
// Heading
$_['heading_title']       = 'Devolução de Produtos';

// Text
$_['text_opened']         = 'Aberto';
$_['text_unopened']       = 'Não Aberto';
$_['text_success']        = 'Devolução atualizada com sucesso!';
$_['text_wait']           = 'Por favor Aguarde!';

// Text
$_['text_return_id']      = 'ID de Devolução:';
$_['text_order_id']       = 'ID de Pedido:';
$_['text_date_ordered']   = 'Data do Pedido:';
$_['text_customer']       = 'Cliente:';
$_['text_email']          = 'E-Mail:';
$_['text_telephone']      = 'Telefone:';
$_['text_return_status']  = 'Situação da Devolução:';
$_['text_comment']        = 'Comentário:';
$_['text_date_added']     = 'Data de Criação:';
$_['text_date_modified']  = 'Data de Modificação:';

// Column
$_['column_return_id']     = 'ID de Devolução';
$_['column_order_id']      = 'ID de Pedido';
$_['column_customer']      = 'Cliente';
$_['column_quantity']      = 'Quantidade';
$_['column_status']        = 'Situação';
$_['column_date_added']    = 'Data Criação';
$_['column_date_modified'] = 'Date Modificação';
$_['column_product']       = 'Produto';
$_['column_model']         = 'Modelo';
$_['column_reason']        = 'Motivo da Devolução';
$_['column_opened']        = 'Aberto';
$_['column_notify']        = 'Cliente notificado';
$_['column_comment']       = 'Comentário';
$_['column_action']        = 'Ação';

// Entry
$_['entry_customer']      = 'Cliente:';
$_['entry_order_id']      = 'ID de Pedido:';
$_['entry_date_ordered']  = 'Data do Pedido:';
$_['entry_firstname']     = 'Nome:';
$_['entry_lastname']      = 'Sobrenome:';
$_['entry_email']         = 'E-Mail:';
$_['entry_telephone']     = 'Telefone:';
$_['entry_product']       = 'Produto:';
$_['entry_model']         = 'Modelo:';
$_['entry_quantity']      = 'Quantidade:';
$_['entry_reason']        = 'Motivo Devolução:';
$_['entry_opened']        = 'Aberto:';
$_['entry_comment']       = 'Comentário:';
$_['entry_return_status'] = 'Situação Devolução:';
$_['entry_notify']        = 'Cliente notificado:';
$_['entry_action']        = 'Ação Devolução:';

// Error
$_['error_warning']       = 'Atenção: Verifique cuidadosamente o formulário para evitar erros!';
$_['error_permission']    = 'Atenção: Você não tem permissão para modificar devoluções!';
$_['error_firstname']     = 'O nome deve ter entre 1 e 32 caracteres!';
$_['error_lastname']      = 'O sobrenome deve ter entre 1 e 32 caracteres!';
$_['error_email']         = 'E-Mail inválido!';
$_['error_telephone']     = 'Telefone deve ter entre 3 e 32 caracteres!';
$_['error_password']      = 'Senha deve ter entre 3 e 32 caracteres!';
$_['error_confirm']       = 'As senhas não coincidem!';
$_['error_address_1']     = 'Endereço deve ter entre 3 e 128 caracteres!';
$_['error_city']          = 'Cidade deve ter entre 3 e 128 caracteres!';
$_['error_postcode']      = 'CEP deve ter 8 caracteres!';
$_['error_country']       = 'Selecione um país!';
$_['error_zone']          = 'Selecione um estado!';
?>