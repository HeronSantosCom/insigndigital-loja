<?php
// Heading
$_['heading_title']      = 'Tema de Vale Presente';

// Text
$_['text_success']       = 'Tema Vale Presente atualizado com sucesso!';
$_['text_image_manager'] = 'Gerenciar imagem';

// Column
$_['column_name']        = 'Nome do Tema Vale Presente';
$_['column_action']      = 'Ação';

// Entry
$_['entry_name']         = 'Nome do Tema Vale Presente:';
$_['entry_description']  = 'Descrição Tema Vale Presente:';
$_['entry_image']        = 'Imagem:';

// Error
$_['error_permission']  = 'Atenção: Você não tem permissão para modificar Tema Vale Presente!';
$_['error_name']        = 'Tema Vale Presente deve ter entre 3 e 32 caracteres!';
$_['error_image']       = 'Imagem necessária!';
$_['error_voucher']     = 'Atenção: Este Tema Vale Presente não pode ser deletado porque está relacionado com %s Vale Presentes!';
?>