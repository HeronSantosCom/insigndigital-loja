<?php
/**
 * @version		$Id: livezilla.php 714 2010-03-11 15:32:10Z mic $
 * @package		FileZilla - Module 4 OpenCart - Language Admin English
 * @copyright	(C) 2010 mic [ http://osworx.net ]. All Rights Reserved.
 * @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

// Heading
$_['heading_title']		= 'LiveZilla';

// Text
$_['text_module']		= 'M&oacute;dulo';
$_['text_success']		= 'M&oacute;dulo LiveZilla editado com sucesso!';
$_['text_left']			= 'Esquerda';
$_['text_right']		= 'Direita';
$_['text_header']		= 'Topo';
$_['text_footer']		= 'Rodap&eacute;';

// Entry
	// standard
$_['entry_status']		= 'Situa&ccedil;&atilde;o';
$_['entry_sort_order']	= 'Ordem';
$_['entry_position']	= 'Posi&ccedil;&atilde;o';
	// mod specific
$_['entry_code']		= 'C&oacute;digo LiveZilla';
$_['entry_header']		= 'Mostrar T&iacute;tulo';
$_['entry_title']		= 'T&iacute;tulo';
$_['entry_yes']			= 'Sim';
$_['entry_no']			= 'N&atilde;o';
$_['entry_visibility']	= 'Visibilidade';

// help - NOTE: tags must be build with entities!!
$_['help_position']		= 'Posi&ccedil;&otilde;es padr&atilde;o s&atilde;o   esquerda e direita, adicionais, dependendo do seu modelo';
$_['help_code']			= 'V&aacute; no painel de administra&ccedil;&atilde;o do Livezilla (gerador de Link), copie o c&oacute;digo gerado e cole aqui';
$_['help_title']		= 'Voc&ecirc; pode definir aqui um t&iacute;tulo para cada idioma. Se v&aacute;zio e a op&ccedil;&atilde;o &quot;Mostrar t&iacute;tulo&quot; &eacute; definida como Sim, a vari&aacute;vel do arquivo de idioma ser&aacute; exibida';
$_['help_visibility']	= 'Digite aqui a URL onde este m&oacute;dulo n&atilde;o ser&aacute; exibido.<br />Ex: E.g. product/product (Produtos), account/account (Cesta)<br />Separe cada URL com uma v&iacute;rgula';

// buttons
$_['button_help']		= 'Ajuda';
$_['button_apply']		= 'Aplicar';

// tabs
$_['tab_common']		= 'Comum';
$_['tab_advanced']		= 'Avan&ccedil;ado';

// Error
$_['error_permission']	= 'Aten&ccedil;&atilde;o: voc&ecirc; n&atilde;o direitos   suficientes para esta a&ccedil;&atilde;o!';
$_['error_code']		= 'C&oacute;digo requerido';