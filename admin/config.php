<?php
// HTTP
define('HTTP_SERVER', 'http://loja.insigndigital.com.br/admin/');
define('HTTP_CATALOG', 'http://loja.insigndigital.com.br/');
define('HTTP_IMAGE', 'http://loja.insigndigital.com.br/image/');

// HTTPS
define('HTTPS_SERVER', 'http://loja.insigndigital.com.br/admin/');
define('HTTPS_IMAGE', 'http://loja.insigndigital.com.br/image/');

// DIR
define('DIR_APPLICATION', '/home/heronreis/loja.insigndigital.com.br/admin/');
define('DIR_SYSTEM', '/home/heronreis/loja.insigndigital.com.br/system/');
define('DIR_DATABASE', '/home/heronreis/loja.insigndigital.com.br/system/database/');
define('DIR_LANGUAGE', '/home/heronreis/loja.insigndigital.com.br/admin/language/');
define('DIR_TEMPLATE', '/home/heronreis/loja.insigndigital.com.br/admin/view/template/');
define('DIR_CONFIG', '/home/heronreis/loja.insigndigital.com.br/system/config/');
define('DIR_IMAGE', '/home/heronreis/loja.insigndigital.com.br/image/');
define('DIR_CACHE', '/home/heronreis/loja.insigndigital.com.br/system/cache/');
define('DIR_DOWNLOAD', '/home/heronreis/loja.insigndigital.com.br/download/');
define('DIR_LOGS', '/home/heronreis/loja.insigndigital.com.br/system/logs/');
define('DIR_CATALOG', '/home/heronreis/loja.insigndigital.com.br/catalog/');

// DB
define('DB_DRIVER', 'mysql');
define('DB_HOSTNAME', 'mysql.insigndigital.com.br');
define('DB_USERNAME', 'insigndigital');
define('DB_PASSWORD', 'insigndigital!rio');
define('DB_DATABASE', 'ips_loja');
define('DB_PREFIX', '');
?>