<?php
/**
 * @version		$Id: livezilla.php 1202 2010-06-29 05:46:12Z mic $
 * @package		FileZilla - Module 4 OpenCart - Admin Controller
 * @copyright	(C) 2010 mic [ http://osworx.net ]. All Rights Reserved.
 * @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

class ControllerModuleLiveZilla extends Controller
{
	private $error		= array();
	private $_name		= '';
	private $_url;
	private $_type		= 'module';
	private $_version	= '1.0.7';

	/**
	 * main function
	 */
	public function index() {
		$this->getBasics();
		$this->getLanguage();

		$this->load->model( 'setting/setting' );

		if( ( $this->request->server['REQUEST_METHOD'] == 'POST' ) && ( $this->validate() ) ) {
			$this->model_setting_setting->editSetting( $this->_name, $this->request->post );

			$this->session->data['success'] = $this->language->get( 'text_success' );

			if( isset( $this->request->post['mode'] ) && $this->request->post['mode'] == 'apply' ) {
				$this->redirect( $this->_url . $this->_type .'/'. $this->_name );
			}else{
				$this->redirect( $this->_url . 'extension/module' );
			}
		}

		$this->data['version']	= $this->_version;

		$this->getLinks();
		$this->getParams();
		$this->getTemplate();
		$this->getBreadcrumbs();
		$this->getMessages();
		$this->getDocument();

		$this->response->setOutput( $this->render( true ), $this->config->get( 'config_compression' ) );
	}

	/**
	 * calls several internal functions to load basic settings/definitions
	 */
	private function getBasics() {
		$this->getName();
		$this->getFooter();

		$this->data['token']	= ( !empty( $this->session->data['token'] ) ? $this->session->data['token'] : '' );
		$this->_url				= HTTPS_SERVER . 'index.php?token=' . $this->data['token'] . '&amp;route=';
	}

    /**
	 * gets the module name out of the class
	 */
	private function getName() {
        $this->_fName	= str_replace( 'Controller', '', get_class( $this ) );
        $this->_fName	= str_replace( ucfirst( $this->_type ), '', $this->_fName );
		$this->_name	= strtolower( $this->_fName );
    }

	/**
	 * - adds javascript and css into document header
	 * - defines document title
	 * @since OC 1.4.8: style has changed!
	 * sample: <link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
	 */
	private function getDocument() {
		$jPath = '';

		// check OC version
		if( defined( 'VERSION' ) ) {
			// ohh, we have OC >= 1.4.8, I love that developers!
			$vers = constant( 'VERSION' );
			$vers = str_replace( '.', '', $vers );

			if( $vers >= '148' ) {
				// define additional path for javascript
				$jPath = 'view/javascript/';
				$this->document->styles = array(
					array( 'rel' => 'stylesheet',	'href' => 'view/stylesheet/tooltip_simple.css',	'media' => 'screen' )
				);
			}

		}else{
			// do it the old way (before 1.4.8)
			$this->document->styles = array(
				'tooltip_simple.css'
			);
		}

		$this->document->scripts = array(
			$jPath . 'jquery/tooltip/jquery.tools_tooltips.min.js'
		);

		$this->document->title		= $this->language->get( 'heading_title' );
	}

	/**
	 * build href links
	 */
	private function getLinks() {
		$this->data['link'] = array(
			'action'	=> $this->_url . $this->_type .'/'. $this->_name,
			'cancel'	=> $this->_url . 'extension/' . $this->_type,
			'help'		=> 'http://osworx.net?task=findkey&amp;keyref=opencart_livezilla'
		);
	}

	/**
	 * build breadcrumbs
	 */
	private function getBreadcrumbs() {
		$this->document->breadcrumbs = array(
			array(
	       		'href'      => $this->_url . 'common/home',
	       		'text'      => $this->language->get( 'text_home' ),
	      		'separator' => false
		  	),
	  		array(
	       		'href'      => $this->_url . 'extension/' . $this->_type,
	       		'text'      => $this->language->get( 'text_module' ),
	      		'separator' => ' :: '
	   		)
 		);
	}

	/**
	 * get locale, active languages
	 */
	private function getLocaleLangs() {
		$this->load->model( 'localisation/language' );
		$this->data['localeLangs'] = $this->model_localisation_language->getLanguages();
	}

	/**
	 * get language vars
	 * @param string	$lng	optional language file to load
	 */
	private function getLanguage( $lng = '' ) {
		if( !$lng ) {
			$langs = $this->load->language( $this->_type .'/'. $this->_name );
		}else{
			$langs = $this->load->language( $lng );
		}

		$this->getLocaleLangs();

		$this->data = array_merge( $this->data, $langs );
	}

	/**
	 * get params
	 */
	private function getParams() {
		$params = array(
			// standard
			'position', 'status', 'sort_order',
			// mod specific
			'code', 'header', 'title', 'visibility'
		);

		foreach( $params as $param ) {
			$this->getParam( '_' . $param );
		}

		// language specific params
		foreach( $this->data['localeLangs'] as $lang ) {
			$this->getParam( '_title' . $lang['language_id'] );
		}

		unset( $params );
	}

	/**
	 * get a value either from request or config
	 */
	private function getParam( $param ) {
		$name = $this->_name . $param;
		if( isset( $this->request->post[$name] ) ) {
			$this->data[$name] = $this->request->post[$name];
		}else{
			$this->data[$name] = $this->config->get( $name );
		}
	}

	/**
	 * define template params
	 */
	private function getTemplate() {
		$this->template = $this->_type .'/'. $this->_name . '.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
	}

	/**
	 * get error messages
	 */
	private function getMessages() {
		$errors = array( 'warning', 'code' );

		foreach( $errors as $error ) {
			if( isset( $this->error[$error] ) ) {
				$this->data['error_' . $error] = $this->error[$error];
			}else{
				$this->data['error_' . $error] = '';
			}
		}

		unset( $errors );

		foreach( $this->data['localeLangs'] as $lang ) {
			if( isset( $this->error['code' . $lang['language_id']] ) ) {
				$this->data['error_code' . $lang['language_id']] = $this->error['code' . $lang['language_id']];
			}else{
				$this->data['error_code' . $lang['language_id']] = '';
			}
		}

		if( isset( $this->session->data['success'] ) ) {
			$this->data['success'] = $this->session->data['success'];
			unset( $this->session->data['success'] );
		}else{
			$this->data['success'] = '';
		}
	}

	/**
	 * constructs the footer
	 *
	 * Note: displaying this footer is mandatory, removing violates the license!
	 * If you do not want to display the footer, contact the author.
	 */
	private function getFooter() {
		$this->data['oxfooter']	= '<div style="text-align:center; color:#666666; margin-top:5px">'
		. $this->_fName
		. ' Module v.' . $this->_version
		. ' &copy; '
		. date( 'Y' )
		. ' by <a href="http://osworx.net" target="_blank">OSWorX</a>'
		. '</div>'
		;
	}

	/**
	 * validates user permission and checks specific module vars
	 */
	private function validate() {
		if( !$this->user->hasPermission( 'modify', $this->_type .'/'. $this->_name ) ) {
			$this->error['warning'] = $this->language->get( 'error_permission' );
		}

		if( !$this->request->post[$this->_name . '_code'] ) {
			$this->error['code'] = $this->language->get( 'error_code' );
		}

		if( !$this->error ) {
			return true;
		}else{
			return false;
		}
	}
}