<?php 
class ControllerPaymentF2b extends Controller {
	private $error = array(); 
	public function index() {
		$this->load->language('payment/f2b');
		$this->document->title = $this->language->get('heading_title');
		$this->load->model('setting/setting');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validate())) {
			$this->load->model('setting/setting');
			$this->model_setting_setting->editSetting('f2b', $this->request->post);				
			$this->session->data['success'] = $this->language->get('text_success');
			$this->redirect(HTTPS_SERVER . 'index.php?route=extension/payment&token=' . $this->session->data['token']);
		}
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_all_zones'] = $this->language->get('text_all_zones');
		$this->data['text_none'] = $this->language->get('text_none');
		$this->data['text_yes'] = $this->language->get('text_yes');
		$this->data['text_no'] = $this->language->get('text_no');
		$this->data['entry_conta'] = $this->language->get('entry_conta');
		$this->data['entry_senha'] = $this->language->get('entry_senha');
		$this->data['entry_prazo'] = $this->language->get('entry_prazo');
		$this->data['entry_demonstrativo1'] = $this->language->get('entry_demonstrativo1');
		$this->data['entry_demonstrativo2'] = $this->language->get('entry_demonstrativo2');
		$this->data['entry_demonstrativo3'] = $this->language->get('entry_demonstrativo3');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');		
		$this->data['entry_order_status'] = $this->language->get('entry_order_status');		
		$this->data['help_conta'] = $this->language->get('help_conta');
		$this->data['help_senha'] = $this->language->get('help_senha');
		$this->data['help_prazo'] = $this->language->get('help_prazo');
		$this->data['help_demonstrativo1'] = $this->language->get('help_demonstrativo1');
		$this->data['help_demonstrativo2'] = $this->language->get('help_demonstrativo2');
		$this->data['help_demonstrativo3'] = $this->language->get('help_demonstrativo3');
		$this->data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['tab_general'] = $this->language->get('tab_general');
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->error['conta'])) {
			$this->data['error_conta'] = $this->error['conta'];
		} else {
			$this->data['error_conta'] = '';
		}
		if (isset($this->error['senha'])) {
			$this->data['error_senha'] = $this->error['senha'];
		} else {
			$this->data['error_senha'] = '';
		}
		if (isset($this->error['prazo'])) {
			$this->data['error_prazo'] = $this->error['prazo'];
		} else {
			$this->data['error_prazo'] = '';
		}
		if (isset($this->error['demonstrativo1'])) {
			$this->data['error_demonstrativo1'] = $this->error['demonstrativo1'];
		} else {
			$this->data['error_demonstrativo1'] = '';
		}
  		$this->document->breadcrumbs = array();
   		$this->document->breadcrumbs[] = array(
       		'href'      => HTTPS_SERVER . 'index.php?route=common/home&token=' . $this->session->data['token'],
       		'text'      => $this->language->get('text_home'),
      		'separator' => FALSE
   		);
   		$this->document->breadcrumbs[] = array(
       		'href'      => HTTPS_SERVER . 'index.php?route=extension/payment&token=' . $this->session->data['token'],
       		'text'      => $this->language->get('text_payment'),
      		'separator' => ' :: '
   		);
   		$this->document->breadcrumbs[] = array(
       		'href'      => HTTPS_SERVER . 'index.php?route=payment/f2b&token=' . $this->session->data['token'],
       		'text'      => $this->language->get('heading_title'),
      		'separator' => ' :: '
   		);
		$this->data['action'] = HTTPS_SERVER . 'index.php?route=payment/f2b&token=' . $this->session->data['token'];
		$this->data['cancel'] = HTTPS_SERVER . 'index.php?route=extension/payment&token=' . $this->session->data['token'];
	if (isset($this->request->post['f2b_status'])) {
			$this->data['f2b_status'] = $this->request->post['f2b_status'];
		} else {
			$this->data['f2b_status'] = $this->config->get('f2b_status'); 
		} 
		if (isset($this->request->post['f2b_conta'])) {
			$this->data['f2b_conta'] = $this->request->post['f2b_conta'];
		} else {
			$this->data['f2b_conta'] = $this->config->get('f2b_conta'); 
		} 
		if (isset($this->request->post['f2b_senha'])) {
			$this->data['f2b_senha'] = $this->request->post['f2b_senha'];
		} else {
			$this->data['f2b_senha'] = $this->config->get('f2b_senha');
		}
		if (isset($this->request->post['f2b_prazo'])) {
			$this->data['f2b_prazo'] = $this->request->post['f2b_prazo'];
		} else {
			$this->data['f2b_prazo'] = $this->config->get('f2b_prazo');
		}
		if (isset($this->request->post['f2b_demonstrativo1'])) {
			$this->data['f2b_demonstrativo1'] = $this->request->post['f2b_demonstrativo1'];
		} else {
			$this->data['f2b_demonstrativo1'] = $this->config->get('f2b_demonstrativo1');
		}
		if (isset($this->request->post['f2b_demonstrativo2'])) {
			$this->data['f2b_demonstrativo2'] = $this->request->post['f2b_demonstrativo2'];
		} else {
			$this->data['f2b_demonstrativo2'] = $this->config->get('f2b_demonstrativo2');
		}
		if (isset($this->request->post['f2b_demonstrativo3'])) {
			$this->data['f2b_demonstrativo3'] = $this->request->post['f2b_demonstrativo3'];
		} else {
			$this->data['f2b_demonstrativo3'] = $this->config->get('f2b_demonstrativo3');
		}
		if (isset($this->request->post['f2b_order_status_id'])) {
			$this->data['f2b_order_status_id'] = $this->request->post['f2b_order_status_id'];
		} else {
			$this->data['f2b_order_status_id'] = $this->config->get('f2b_order_status_id'); 
		} 
		$this->load->model('localisation/order_status');
		$this->data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
		if (isset($this->request->post['f2b_geo_zone_id'])) {
			$this->data['f2b_geo_zone_id'] = $this->request->post['f2b_geo_zone_id'];
		} else {
			$this->data['f2b_geo_zone_id'] = $this->config->get('f2b_geo_zone_id'); 
		} 
		$this->load->model('localisation/geo_zone');
		$this->data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();
		if (isset($this->request->post['f2b_status'])) {
			$this->data['f2b_status'] = $this->request->post['f2b_status'];
		} else {
			$this->data['f2b_status'] = $this->config->get('f2b_status');
		}
		if (isset($this->request->post['f2b_sort_order'])) {
			$this->data['f2b_sort_order'] = $this->request->post['f2b_sort_order'];
		} else {
			$this->data['f2b_sort_order'] = $this->config->get('f2b_sort_order');
		}
		$this->template = 'payment/f2b.tpl';
		$this->children = array(
			'common/header',	
			'common/footer'	
		);
		
		$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
	}
	private function validate() {
		if (!$this->user->hasPermission('modify', 'payment/f2b')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		if (!$this->request->post['f2b_conta']) {
			$this->error['conta'] = $this->language->get('error_conta');
		}
		if (!$this->request->post['f2b_senha']) {
			$this->error['senha'] = $this->language->get('error_senha');
		}
		if (!$this->request->post['f2b_prazo']) {
			$this->error['prazo'] = $this->language->get('error_prazo');
		}	
		if (!$this->request->post['f2b_demonstrativo1']) {
			$this->error['demonstrativo1'] = $this->language->get('error_demonstrativo1');
		}			
		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}	
	}
}
?>
