<?php
// Heading 
$_['heading_title']         = 'Histórico do Pedido';

// Text
$_['text_account']          = 'Conta';
$_['text_order']            = 'Informação de pedido';
$_['text_order_detail']     = 'Detalhe de pedido';
$_['text_invoice_no']       = 'Fatura nº:';
$_['text_order_id']         = 'ID de pedido:';
$_['text_status']           = 'Status:';
$_['text_date_added']       = 'Adicionado em:';
$_['text_customer']         = 'Cliente:';
$_['text_shipping_address'] = 'Endereço de entrega';
$_['text_shipping_method']  = 'Método de entrega:';
$_['text_payment_address']  = 'Endereço de pagamento';
$_['text_payment_method']   = 'Método de pagamento:';
$_['text_products']         = 'Produtos:';
$_['text_total']            = 'Total:';
$_['text_comment']          = 'Comentários do pedido';
$_['text_history']          = 'Histórico do pedido';
$_['text_empty']            = 'Você ainda não fez nenhum pedido!';
$_['text_error']            = 'O pedido que você está procurando não foi encontrado!';
$_['text_action']           = 'Escolha uma ação:';
$_['text_selected']         = 'Com selecionados..';
$_['text_reorder']          = 'Adicionar ao carrinho';
$_['text_return']           = 'Produtos devolvidos';


// Column
$_['column_name']           = 'Nome do produto';
$_['column_model']          = 'Modelo';
$_['column_quantity']       = 'Quantidade';
$_['column_price']          = 'Preço';
$_['column_total']          = 'Total';
$_['column_date_added']     = 'Adicionado em';
$_['column_status']         = 'Status';
$_['column_comment']        = 'Comentário';

$_['error_warning']         = 'Atenção: Você deve selecionar alguns produtos e algumas ações para completar sua requisição!';
?>
