<?php
// Heading  
$_['heading_title']   = 'Meu Carrinho de Compras';

// Text
$_['text_weight']  	  = 'Peso total:';
$_['text_points']     = 'Pontos de fidelidade: %s';
$_['text_items']      = '%s item(s) - %s';
$_['text_success']    = 'Você adicionou <a href="%s">%s</a> em seu <a href="%s">carrinho de compras</a>!';
$_['text_empty']      = 'Seu carrinho está vazio!';
$_['text_login']      = 'Atenção: Você deve <a href="%s">efetuar login</a> ou <a href="%s">criar uma conta</a> para visualizar preços!';

// Column
$_['column_remove']   = 'Remover';
$_['column_image']    = 'Imagem';
$_['column_name']     = 'Nome';
$_['column_model']    = 'Modelo';
$_['column_quantity'] = 'Quantidade';
$_['column_price']    = 'Valor unitário';
$_['column_total']    = 'Total';


// Error
$_['error_stock']     = 'Produtos marcados com *** não estão disponíveis nas quantias desejadas ou não encontram-se em estoque!';
$_['error_minimum']   = 'Quantidade mínima para %s é %s!';	
$_['error_required']  = '%s obrigatório!';	
?>