<?php
// Heading
$_['heading_title'] = 'Confirmado!';

// Text
// versão antiga opencart
$_['text_message']  = '<p>Seu pedido foi confirmado com sucesso!</p><p>Caso tenha criado uma conta você pode ver o histórico dos seus pedidos acessando sua "<a href="%s">Conta</a>" e clicando em "<a href="%s">Histórico de pedidos</a>".</p><p>Por favor, entre em contato conosco caso tenha dúvidas clicando <a href="%s">aqui</a>.</p><p>Obrigado por comprar em nossa loja!</p><br />';

$_['text_customer'] = '<p>Seu pedido foi confirmado com sucesso!</p><p>Caso tenha criado uma conta você pode ver o histórico dos seus pedidos acessando sua "<a href="%s">Conta</a>" e clicando em "<a href="%s">Histórico de pedidos</a>".</p><p>Por favor, entre em contato conosco caso tenha dúvidas clicando <a href="%s">aqui</a>.</p><p>Obrigado por comprar em nossa loja!</p><br />';
$_['text_guest']    = '<p>Seu pedido foi confirmado com sucesso!</p><p>Por favor, entre em contato conosco caso tenha dúvidas clicando <a href="%s">aqui</a>.</p><p>Obrigado por comprar em nossa loja!</p>';
$_['text_basket']   = 'Carrinho';
$_['text_checkout'] = 'Finaçização';
$_['text_success']  = 'Confirmado';
?>