<?php
// Text
$_['text_home']     = 'Home';
$_['text_wishlist'] = 'Lista de desejos (%s)';
$_['text_cart']     = 'Carrinho';
$_['text_items']    = '%s item(s) - %s';
$_['text_search']   = 'Busca';
$_['text_welcome']  = 'Bem vindo, visitante. Faça seu <a href="%s">login</a> ou <a href="%s">Cadastre-se</a>.';
$_['text_logged']   = 'Você está logado como <a href="%s">%s</a> <b>(</b> <a href="%s">Sair</a> <b>)</b>';
$_['text_account']  = 'Minha conta';
$_['text_checkout'] = 'Finalizar';
$_['text_language'] = 'Linguagem';
$_['text_currency'] = 'Moeda';
?>