<?php
// Text
$_['text_information']  = 'Informação';
$_['text_service']      = 'Atendimento';
$_['text_extra']        = 'Extras';
$_['text_account']      = 'Conta';
$_['text_contact']      = 'Contate-nos';
$_['text_return']       = 'Devoluções';
$_['text_sitemap']      = 'Mapa do site';
$_['text_manufacturer'] = 'Fabricantes';
$_['text_voucher']      = 'Vale Presente';
$_['text_affiliate']    = 'Afiliados';
$_['text_special']      = 'Promoções';
$_['text_account']      = 'Minha Conta';
$_['text_order']        = 'Histórico de Pedido';
$_['text_wishlist']     = 'Lista de Desejos';
$_['text_newsletter']   = 'Newsletter';
$_['text_powered']      = 'Distribuido por <a href="http://www.insigndigital.com.br">Insign Digital</a><br /> %s &copy; %s';
?>