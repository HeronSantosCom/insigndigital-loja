<?php
// Heading
$_['heading_title'] = 'Sua conta de afiliado foi criada com sucesso!';

// Text 
$_['text_approval'] = '<p>Obrigado por registrar ma conta de afiliado com %s!</p><p>Você será notificado por e-mail assim que sua conta for ativada.</p><p>Se você tiver alguma dúvida sobre o funcionamento deste sistema de afiliados, entre em <a href="%s">contato</a>.</p>';
$_['text_account']  = 'Conta';
$_['text_success']  = 'Sucesso';
?>