<?php
// Heading 
$_['heading_title'] = 'Sair da minha conta';

// Text
$_['text_message']  = '<p>Você foi desconectado da sua conta.</p><p>Seu carrinho de compras foi salvo, e os itens dentro dele serão restaurados assim que você fazer o login novamente.</p>';
$_['text_account']  = 'Conta';
$_['text_logout']   = 'Logout'; 
?>