<?php
// Heading 
$_['heading_title']                = 'Acessar/Cadastrar';

// Text
$_['text_account']                 = 'Conta';
$_['text_login']                   = 'Acessar/Cadastrar';
$_['text_description']              = '<p>%s o programa de afiliados é gratuito e permite que os membros recebam comissões ao colocar links com anúncios em seu website %s ou produtos específicos nele. Todas as vendas feitas para clientes que clicaram em seus links vão lhe proporcionar uma comissão de afiliado. A atual taxa de comissão é %s. </p><p>Para mais informações, visite a nossa página de Perguntas Frequentes ou consulte os nossos Termos &amp; Condições de Afiliado.</p>';
$_['text_new_affiliate']            = 'Novo Afiliado';
$_['text_register_account']         = '<p>Ainda não sou um afiliado.</p><p>Clique em Continue abaixo para criar uma nova conta de afiliado. ATENÇÃO: Esta conta não tem nenhum vínculo com sua conta de cliente!</p>';
$_['text_returning_affiliate']      = 'Login de Afiliado';
$_['text_i_am_returning_affiliate'] = 'Já sou afiliado.';
$_['text_forgotten']                = 'Esqueci minha senha';

// Entry
$_['entry_email']                   = 'E-mail:';
$_['entry_password']                = 'Senha:';

// Error
$_['error_login']                   = 'Erro: Nenhuma correspondência para endereço de email e/ou senha.';
?>