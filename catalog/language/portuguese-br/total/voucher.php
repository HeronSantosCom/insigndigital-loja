<?php
// Heading 
$_['heading_title'] = 'Usar Vale Presente';

// Text
$_['text_voucher']  = 'Vale Presente(%s)';
$_['text_success']  = 'O Vale Presente foi aplicado com sucesso!';

// Entry
$_['entry_voucher'] = 'Insira o código de seu Vale Presente aqui:';

// Error
$_['error_voucher'] = 'Atenção: O Vale Presente é inválido ou o peso-limite foi ultrapassado.';
?>