<?php
// Heading 
$_['heading_title'] = 'Aplicar cupom de desconto';

// Text
$_['text_coupon']   = 'Cupom(%s)';
$_['text_success']  = 'O desconto de cupom foi aplicado com sucesso!';

// Entry
$_['entry_coupon']  = 'Insira o código de cupom aqui:';

// Error
$_['error_coupon']  = 'Atenção: o cupom é inválido, expirou, ou atingiu seu limite de uso!';
?>