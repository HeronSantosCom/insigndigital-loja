<?php
// Heading 
$_['heading_title'] = 'Usar pontos de fidelidade (%s disponíveis)';

// Text
$_['text_reward']   = 'Pontos de fidelidade(%s)';
$_['text_order_id'] = 'ID de pedido: #%s';
$_['text_success']  = 'Seus pontos de fidelidade foram aplicados com sucesso!';

// Entry
$_['entry_reward']  = 'Pontos para usar (Máximo %s):';

// Error
$_['error_empty']   = 'Atenção: Por favor, indique a quantidade de pontos que você deseja usar!';
$_['error_points']  = 'Atenção: Você não tem %s pontos de fidelidade!';
$_['error_maximum'] = 'Atenção: O número máximo de pontos que podem ser aplicados é %s!';
?>