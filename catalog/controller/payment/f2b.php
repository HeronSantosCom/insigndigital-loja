<?php
class ControllerPaymentF2b extends Controller {
  protected function index() {
    $this->data['button_confirm'] = $this->language->get('button_confirm');
	$this->data['button_back'] = $this->language->get('button_back');
	$this->load->model('checkout/order');
	$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
	$this->data['conta_f2b'] = $this->config->get('f2b_conta');
	$this->data['senha_f2b'] = $this->config->get('f2b_senha');
	$this->data['prazo_f2b'] = $this->config->get('f2b_prazo');
	$this->data['demonstrativo1_f2b'] = $this->config->get('f2b_demonstrativo1');
	$this->data['demonstrativo2_f2b'] = $this->config->get('f2b_demonstrativo2');
	$this->data['demonstrativo3_f2b'] = $this->config->get('f2b_demonstrativo3');
	$this->load->library('encryption');
	$encryption = new Encryption($this->config->get('config_encryption'));
	$this->data['ref'] = base64_encode($encryption->encrypt($order_info['order_id'])); 
	$this->data['currency_code'] = $order_info['currency']; 
	$this->data['item_name'] = $this->config->get('config_store'); 
	$this->data['amount'] = $this->currency->format($order_info['total'], $order_info['currency'], $order_info['value'], FALSE); // Valor Total do pedido
	$this->data['pg_contact_firstname'] = html_entity_decode($order_info['payment_firstname'], ENT_QUOTES, 'UTF-8'); // Nome do cliente
	$this->data['pg_contact_last_name'] = html_entity_decode($order_info['payment_lastname'], ENT_QUOTES, 'UTF-8');  // Sobrenome do Cliente
	$this->data['pg_contact_address1'] =  html_entity_decode($order_info['payment_address_1'], ENT_QUOTES, 'UTF-8'); // Endereço
	$this->data['pg_contact_address2'] =  html_entity_decode($order_info['payment_address_2'], ENT_QUOTES, 'UTF-8'); // Complemento
	// Caso tenha adicionado esses campos no cadastro
	if(isset($order_info['payment_numero'])){
      $this->data['pg_contact_numero'] = $order_info['payment_numero'];
	}
	if(isset($order_info['payment_bairro'])){
	  $this->data['pg_contact_bairro'] = $order_info['payment_bairro'];
	}
	$this->data['pg_contact_city'] = html_entity_decode($order_info['payment_city'], ENT_QUOTES, 'UTF-8');
	$this->data['pg_contact_zip'] = html_entity_decode($order_info['payment_postcode'], ENT_QUOTES, 'UTF-8');
	$this->data['pg_contact_country'] = html_entity_decode($order_info['payment_country'], ENT_QUOTES, 'UTF-8');
	$this->data['pg_contact_email'] = $order_info['email'];
	$this->data['invoice'] = $this->session->data['order_id'] . ' - ' . $order_info['payment_firstname'] . ' ' . $order_info['payment_lastname']; 
	// Pega id do país		
    $this->load->model('localisation/country');
    $paises = $this->model_localisation_country->getCountries();		
	foreach ($paises as $country) {
	  if($country['name']==$order_info['payment_country']){
        $codigodopais = $country['country_id'];
      }
	}
	// Com a id do país pega code da cidade
    $this->load->model('localisation/zone');
   	$results = $this->model_localisation_zone->getZonesByCountryId($codigodopais);
    foreach ($results as $result) {
      if($result['name']==$order_info['payment_zone']){
		$this->data['estado'] =$result['code'];
	  }
   	} 
	if(isset($order_info['ddd'])){
	  $this->data['ddd'] = $order_info['ddd'];
	}else{
	  $ntelefone = preg_replace("/[^0-9]/", "", $order_info['telephone']);	
	  if(strlen($ntelefone) >= 10){	
		$ntelefone = ltrim($ntelefone, "0");
		$this->data['ddd'] = substr($ntelefone, 0, 2);
		$this->data['telephone'] = substr($ntelefone, 2,11);
	  }else{
		$this->data['telephone'] = substr($ntelefone, 2,11);
	  }
	}
	// Verifica se tem cupom de desconto		
	if (isset($this->session->data['coupon'])) {
	  $this->load->model('checkout/coupon');
	  $coupon = $this->model_checkout_coupon->getCoupon($this->session->data['coupon']);
	  if(isset($coupon)){	
		$trancaproduto=1;
		$restocupom=$coupon['discount'];
	    if($coupon['shipping']==1){
		  $this->data['fretegratis'] = true;
		}
	  }
	} 		
    // Faz a listagem dos produtos
    $this->data['products'] = array();
	foreach ($this->cart->getProducts() as $product)
    {
	  $option_data = array();
	  foreach ($product['option'] as $option)
	  {
        $option_data[] = array('name'  => $option['name'], 'value' => $option['value']);
      } 
	  if(isset($coupon)){	
	    if($coupon['type']=='P'){
		  $valorddescon = ($restocupom/100)*$product['price'];
		  $valorproduto=($product['price']-$valorddescon);
	    }else if ($coupon['type']=="F"){
	      if($trancaproduto==1){
		    if($restocupom<=$product['price']){
			  $valorproduto=($product['price']-$restocupom);
			  $trancaproduto=2;
		    } else {
			  $restocupom=($restocupom-$product['price']);	
			  $valorproduto=0;
		    }
		  }else {
		    $valorproduto=$product['price'];
		  }
	    }
	  }else {	
	    $valorproduto=$product['price'];
	  }	
	  $this->data['products'][] = array('descricao' => $product['name'], 'valor' => $this->currency->format($this->tax->calculate($valorproduto, $product['tax_class_id'], $this->config->get('config_tax'))), 'quantidade' => $product['quantity'], 'option' => $option_data, 'id' => $product['product_id'],	 'peso' => $this->weight->format($this->cart->getWeight(), $this->config->get('config_weight_class')) 		); 
	} 
    // Fim da listagem		
    $this->data['action'] = 'https://www.f2b.com.br/BillingWeb';
    $this->data['back'] = HTTPS_SERVER . 'index.php?route=checkout/payment';
    $this->data['continue'] = HTTPS_SERVER . 'index.php?route=checkout/success';
    if ($this->request->get['route'] != 'checkout/guest_step_3') {
 	 $this->data['back'] = HTTPS_SERVER . 'index.php?route=checkout/payment';
    } else {
      $this->data['back'] = HTTPS_SERVER . 'index.php?route=checkout/guest_step_2';
    }	
    $this->id = 'payment';
    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/f2b.tpl')) {
	  $this->template = $this->config->get('config_template') . '/template/payment/f2b.tpl';
	} else {
	  $this->template = 'default/template/payment/f2b.tpl';
	}	
	$this->render();
  }
  public function confirm() {
    $this->language->load('payment/f2b');
    $this->load->model('checkout/order');
    $comment  = $this->language->get('text_instruction') . "\n\n";
    $comment .= $this->language->get('text_payment');
    $comment .= $this->language->get('text_payment');
    $this->model_checkout_order->confirm($this->session->data['order_id'], $this->config->get('f2b_order_status_id'), $comment);
    // Limpa a sessão
    if (isset($this->session->data['order_id'])) { 
      $this->cart->clear();
      unset($this->session->data['shipping_method']);
	  unset($this->session->data['shipping_methods']);
	  unset($this->session->data['payment_method']);
	  unset($this->session->data['payment_methods']);
	  unset($this->session->data['comment']);
	  unset($this->session->data['order_id']);	
	  unset($this->session->data['coupon']);
    }
  }	
}
?>
