<form action="<?php echo $action; ?>" method="post" id="formf2b">
  <input type="hidden" name="conta" value="<?php echo $conta_f2b; ?>" />
  <input type="hidden" name="senha" value="<?php echo $senha_f2b; ?>" />
  <input type="hidden" name="tipo_ws" value="OpenCart" />
  <input type="hidden" name="grupo" value="OpenCart" />
  <input type="hidden" name="num_document" value="<?PHP echo $ref; ?>" />
  <input type="hidden" name="demonstrativo_1" value="<?php echo $demonstrativo1_f2b; ?>" />
  <input type="hidden" name="demonstrativo_2" value="<?php echo $demonstrativo2_f2b; ?>" />
  <input type="hidden" name="demonstrativo_3" value="<?php echo $demonstrativo3_f2b; ?>" />
  <input type="hidden" name="valor" value="<?PHP echo $amount; ?>" />
  <input type="hidden" name="prazo_vencimento" value="<?php echo $prazo_f2b; ?>" />
  <input type="hidden" name="nome" value="<?PHP echo $pg_contact_firstname.' '.$pg_contact_last_name; ?>" />
  <input type="hidden" name="email_1" value="<?PHP echo $pg_contact_email; ?>" />
  <input type="hidden" name="endereco_cep" value="<?PHP echo $pg_contact_zip; ?>" />
  <input type="hidden" name="endereco_logradouro" value="<?PHP echo $pg_contact_address1; ?>" />
  <?php if (isset($numero)) {
	echo "<input type='hidden' name='endereco_numero' value='".$numero."' />\n";
  } else {
	echo "<input type='hidden' name='endereco_numero' value='0' />\n";
  }?>
  <input type="hidden" name="endereco_complemento" value="<?PHP echo $pg_contact_address2; ?>" />
  <?php if (isset($bairro)) {
	echo "<input type='hidden' name='endereco_bairro' value='".$bairro."' />\n";
  } else {
	echo "<input type='hidden' name='endereco_bairro' value='' />\n";
  }?>
  <input type="hidden" name="endereco_cidade" value="<?PHP echo $pg_contact_city; ?>" />
  <input type="hidden" name="endereco_estado" value="<?PHP echo $estado; ?>" />
  <input type="hidden" name="telefone_ddd" value="<?PHP echo $ddd; ?>" />
  <input type="hidden" name="telefone_numero" value="<?PHP echo $telephone; ?>" />
</form>

<div style="background: #F7F7F7; border: 1px solid #DDDDDD; padding: 10px; margin-bottom: 10px;">
<br />
<center><img src="https://f2link.f2b.com.br/pedido/images/BannerPgto.jpg" alt="F2b" width="500" height="60" border="0" />
</center>
<br />
Após clicar no botão <strong>F2b Comprar</strong> que está abaixo, voc&ecirc; será redirecionado para o F2b para efetuar o pagamento (<strong>Libere antes de continuar o bloqueador de Popup para o site www.f2b.com.br</strong>).
<br />
<br />
</div>

<div class="buttons">
  <table>
    <tr>
      <td align="left"><a onclick="location = '<?php echo str_replace('&', '&amp;', $back); ?>'" class="button"><span><?php echo $button_back; ?></span></a></td>
      <td align="right"><a id="checkout" ><span><img src="https://f2link.f2b.com.br/pedido/images/button/botaocomprarcinza.gif" border="0" /></span></a></td>
      </tr>
  </table>
</div>

<script type="text/javascript"><!--
$('#checkout').click(function() {
 $('body').css("cursor", "wait");
$('#checkout').hide('fast');
	$.ajax({ 
		type: 'GET',
		url: 'index.php?route=payment/f2b/confirm',
		success: function() {
			 $('#formf2b').submit();
		}		
	});
});
//--></script>