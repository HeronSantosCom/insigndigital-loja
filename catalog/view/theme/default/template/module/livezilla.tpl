<?php
/**
 * @version		$Id: livezilla.tpl 714 2010-03-11 15:32:10Z mic $
 * @package		FileZilla - Module 4 OpenCart - Language Admin German
 * @copyright	(C) 2010 mic [ http://osworx.net ]. All Rights Reserved.
 * @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */
?>

<div class="box">
	<div class="top">
	<?php
	if( $title ) { ?>
		<img src="catalog/view/theme/default/image/chat.png" width="16" height="16" alt="" /><?php echo $title; ?>
		<?php
	} ?>
	</div>
	<div class="middle" style="text-align: center;">
    	<?php echo $code; ?>
	</div>
	<?php echo $oxfooter; ?>
	<div class="bottom">&nbsp;</div>
</div>